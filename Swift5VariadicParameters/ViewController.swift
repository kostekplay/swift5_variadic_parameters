////  ViewController.swift
//  Swift5VariadicParameters
//
//  Created on 29/09/2020.
//  
//

import UIKit

class ViewController: UIViewController {

    private let field: UITextField = {
        let f = UITextField()
        f.placeholder = "Placeholder"
        return f
    }()
    
    private let button: UIButton = {
        let b = UIButton()
        b.backgroundColor = .red
        return b
    }()
    
    private let label: UILabel = {
        let l = UILabel()
        l.text = "Label"
        l.backgroundColor = .green
        return l
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = .gray
        
//        view.addSubview(field)
//        view.addSubview(button)
//        view.addSubview(label)
        
        view.addSubviews(field, label, button)
        
        var i = 0
        for subview in view.subviews {
            subview.frame = CGRect(x: 10, y: 100 * i, width: 100, height: 100)
            i += 1
        }
        
    }
}

extension UIView {
    func addSubviews(_ views: UIView...) {
        for view in views {
            addSubview(view)
        }
    }
}

